# Faculty of 1000 - Front-end task

Fork this project, then use the API details in the 'docs' folder to access the F1000 API. An API key has been created for an account which has a number of associated references.

Write a simple React web app that can display all the references from the API, in groups of 10 at a time.

You should include a facility to sort by *title* and by *publishedDate* (as a minimum) in both ascending and descending orders.

There is a lot of information to display, particularly on a mobile screen, so think about your layout carefully!

Publish your web app to the internet: GitLab pages may be easiest, an example CI file is included.

### CORS issues?

At the time of writing the F1000 external API does not allow CORS requests from arbitrary hosts. This may be fixed by the time you attempt the task, but if not use the CORS proxy in the 'proxy' directory. It will listen on port 3001 and proxy all your requests to the F1000 server.
