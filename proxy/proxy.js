const url = require('url');
const URL = url.URL;
const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const chalk = require('chalk');

const app = express();

app.use(bodyParser.json());

app.all('/extapi/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
    res.header("Access-Control-Allow-Headers", req.header('access-control-request-headers'));

    if (req.method === 'OPTIONS') {
        console.log("preflight for ", req.url);
        res.send(); // CORS preflight response
    } else {
        const targetPath = req.path;
        const targetHost = 'https://f1000.com/';
        const targetURL = new URL(targetPath, targetHost);

        console.log(`request ${req.path} proxied to ${targetURL}`);
        console.log(req.headers['authorization']);

        request({
            url: targetURL,
            method: req.method,
            json: req.body,
            headers: {
                'Authorization': req.headers['authorization'].replace(/bearer/, 'Bearer'),
                //'Authorization': 'Bearer B9131B6213FF8B531DD62E54D67C70A2',
                'User-Agent': 'f1000-proxy/1.0.1',
                'Accept': '*/*'
            }
        },
        function (error, response, body) {
            if (error) {
                console.error(chalk.red('error: ' + response.statusCode));
            } else {
                console.log(chalk.green(JSON.stringify(response, null, 4)));
            }
        }).pipe(res);
    }
});

app.set('port', process.env.PORT || 3001);

app.listen(app.get('port'), function () {
    console.log('proxy listening on port ' + app.get('port'));
});
