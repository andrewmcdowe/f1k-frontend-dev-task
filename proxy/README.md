# F1000 CORS Proxy

If you're getting blocked requests because of CORS issues when communicating directly with the F1000 API, you can use this proxy.

Install dependencies with `yarn install`, then run the proxy with `yarn script start`, and point your API requests at 'http://localhost:3001/extapi/work/' rather than 'http://f1000.com/extapi/work'. The proxy will forward any 'Authorization' headers so your API key should still work.


